package test;

import main.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestCommand {
    @Test
    public void testCommandPizza(){
        Receiver receiver = new Receiver();
        Command orderPizza = new OrderAPizzaCommand(receiver);
        Command orderBurger = new OrderABurgerCommand(receiver);
        Invoker invoker = new Invoker(orderPizza, orderBurger);
        assertEquals("Вам принесли пиццу", invoker.orderPizza());
    }

    @Test
    public void testCommandBurger(){
        Receiver receiver = new Receiver();
        Command orderPizza = new OrderAPizzaCommand(receiver);
        Command orderBurger = new OrderABurgerCommand(receiver);
        Invoker invoker = new Invoker(orderPizza, orderBurger);
        assertEquals("Вам принесли бургер", invoker.orderBurger());
    }
}
