package main;

public class OrderABurgerCommand implements Command {
    private Receiver receiver;

    public OrderABurgerCommand(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public String execute() {
        return receiver.orderABurger();
    }
}
