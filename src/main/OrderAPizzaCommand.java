package main;

public class OrderAPizzaCommand implements Command{
    private Receiver receiver;

    public OrderAPizzaCommand(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public String execute() {
        return receiver.orderAPizza();
    }
}
