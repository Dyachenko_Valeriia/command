package main;

public class Invoker {
    private Command orderAPizzaCommand;
    private Command orderABurgerCommand;

    public Invoker(Command orderAPizzaCommand, Command orderABurgerCommand) {
        this.orderAPizzaCommand = orderAPizzaCommand;
        this.orderABurgerCommand = orderABurgerCommand;
    }

    public String orderPizza(){
        return orderAPizzaCommand.execute();
    }

    public String orderBurger(){
        return orderABurgerCommand.execute();
    }
}
