package main;

public interface Command {
    String execute();
}
